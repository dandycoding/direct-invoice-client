require "configurations"
require "direct_invoice/version"

require "direct_invoice/errors/direct_invoice_error"
require "direct_invoice/errors/record_not_found"
require "direct_invoice/errors/record_invalid"
require "direct_invoice/errors/sending_error"
require "direct_invoice/errors/request_error"

require "direct_invoice/api_operations/base"
require "direct_invoice/api_operations/read"
require "direct_invoice/api_operations/write"
require "direct_invoice/api_operations/all"

require "direct_invoice/base"
require "direct_invoice/company"
require "direct_invoice/document_line"
require "direct_invoice/document"
require "direct_invoice/invoice"
require "direct_invoice/template"

module DirectInvoice

  include Configurations
  configurable :api_key

  not_configured :api_key do |prop|
    raise NoMethodError, "#{api_key} must be configured"
  end

  def self.root
    File.expand_path '../..', __FILE__
  end

end
