class DirectInvoice::Template < DirectInvoice::Base

  include DirectInvoice::APIOperations::Read

  ATTRIBUTES = [:id, :name, :locales, :for_invoice, :for_estimate, :for_credit_note, :created_at, :updated_at]
  attr_accessor *ATTRIBUTES

  #
  # Instance methods
  #

  def initialize(attributes)
    self.attributes = attributes
  end

end
