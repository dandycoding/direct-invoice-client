module DirectInvoice
  module APIOperations
    module Base

      attr_accessor :errors

      module ClassMethods
        def collection_name
          self.to_s.demodulize.pluralize.underscore
        end
      end

      def self.included(base)
        base.include(HTTParty)
        base.base_uri('https://app.direct-invoice.com/api/v1')
        base.extend(ClassMethods)
      end
    end
  end
end
