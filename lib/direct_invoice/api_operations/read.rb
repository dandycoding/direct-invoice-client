module DirectInvoice
  module APIOperations
    module Read

      attr_accessor :errors

      module ClassMethods

        def find(id)
          response = self.get("/#{self.collection_name}/#{id}?api_key=#{DirectInvoice.configuration.api_key}")
          if response.code == 200
            self.new(response.parsed_response)
          elsif response.code == 404
            raise DirectInvoice::RecordNotFound.new(self)
          end
        end

        def all
          response = self.get("/#{self.collection_name}?api_key=#{DirectInvoice.configuration.api_key}")
          response.parsed_response.map{ |obj| self.new(obj) }
        end

      end

      def self.included(base)
        base.include(DirectInvoice::APIOperations::Base)
        base.extend(ClassMethods)
      end
    end
  end
end
