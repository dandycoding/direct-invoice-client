module DirectInvoice
  module APIOperations
    module Write

      module ClassMethods
        def create(attributes)
          obj = self.new(attributes)
          obj.save
          obj
        end
      end

      def self.included(base)
        base.include(DirectInvoice::APIOperations::Base)
        base.extend(ClassMethods)
      end

      def save
        if !self.id.present?
          response = self.class.post("/#{self.class.collection_name}?api_key=#{DirectInvoice.configuration.api_key}", body: self.attributes)
        else
          response = self.class.put("/#{self.class.collection_name}/#{self.id}?api_key=#{DirectInvoice.configuration.api_key}", body: self.attributes)
        end
        if response.code == 200 || response.code == 201
          self.attributes = response.parsed_response
          true
        else
          self.errors = response.parsed_response["error"]
          raise DirectInvoice::RecordInvalid.new(record: self)
        end
      end
    end
  end
end
