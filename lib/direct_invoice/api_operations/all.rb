module DirectInvoice
  module APIOperations
    module All

      def self.included(base)
        base.include(DirectInvoice::APIOperations::Base)
        base.include(DirectInvoice::APIOperations::Read)
        base.include(DirectInvoice::APIOperations::Write)
      end

    end
  end
end
