module DirectInvoice
  class SendingError < DirectInvoiceError

    attr_reader :message

    def initialize(message)
      @message = message
    end

  end
end
