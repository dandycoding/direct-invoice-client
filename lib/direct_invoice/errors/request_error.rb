module DirectInvoice
  class RequestError < DirectInvoiceError

    attr_reader :errors

    def initialize(errors)
      @errors = errors
    end

  end
end
