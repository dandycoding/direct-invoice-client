class DirectInvoice::Invoice < DirectInvoice::Document

  include DirectInvoice::APIOperations::All

  ATTRIBUTES = [:id, :company_id, :status, :name, :number, :issue_date, :due_date, :currency, :locale, :template_id, :discount, :pdf_link, :prices_encoded_with_vat]
  attr_accessor *ATTRIBUTES

  #
  # Instance methods
  #

  def mark_as_paid
    response = self.class.patch("/#{self.class.collection_name}/#{self.id}/mark_as_paid?api_key=#{DirectInvoice.configuration.api_key}")
    if response.code == 200
      true
    else
      raise DirectInvoice::RequestError.new(response.parsed_response["error"])
    end
  end

  def mark_as_sent
    response = self.class.patch("/#{self.class.collection_name}/#{self.id}/mark_as_sent?api_key=#{DirectInvoice.configuration.api_key}")
    if response.code == 200
      true
    else
      raise DirectInvoice::RequestError.new(response.parsed_response["error"])
    end
  end

end
