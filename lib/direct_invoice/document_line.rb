class DirectInvoice::DocumentLine < DirectInvoice::Base

  ATTRIBUTES = [:description, :unit_price, :quantity, :vat_rate, :divider]
  attr_accessor *ATTRIBUTES

  #
  # Instance methods
  #

  def initialize(attributes)
    self.attributes = attributes
  end

end
