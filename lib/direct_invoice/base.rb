class DirectInvoice::Base

  def attributes=(attributes)
    self.class::ATTRIBUTES.each{ |a| self.send("#{a}=", attributes[a] || attributes[a.to_s]) if attributes.has_key?(a) || attributes.has_key?(a.to_s) }
  end

  def attributes
    attributes = { }
    self.class::ATTRIBUTES.each do |k|
      attributes[k] = send(k) if send(k).present?
    end
    attributes
  end

end
