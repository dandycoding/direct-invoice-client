class DirectInvoice::Document < DirectInvoice::Base

  include DirectInvoice::APIOperations::All

  attr_reader :lines

  #
  # Instance methods
  #

  def initialize(attributes)
    self.attributes = attributes
    self.lines = []
  end

  def lines=(lines)
    @lines = lines.map{ |line| DirectInvoice::DocumentLine.new(line) }
  end

  def attributes
    attributes = super
    attributes[:lines] = self.lines.map{ |line| line.attributes }
    attributes
  end

  def send_by_email(params)
    if self.id.nil?
      raise DirectInvoice::SendingError.new("Cannot send an unsaved document")
    else
      self.class.post("/#{self.class.collection_name}/#{self.id}/send?api_key=#{DirectInvoice.configuration.api_key}", body: params)
    end
  end

end
