class DirectInvoice::Company < DirectInvoice::Base

  include DirectInvoice::APIOperations::All

  ATTRIBUTES = [:id, :name, :email, :country_code, :default_vat_rate, :number, :vat_number, :phone_number, :address_line_1, :address_line_2, :zip_code, :city, :default_credit_note_bottom_text, :default_credit_note_top_text, :default_credit_note_email_subject, :default_credit_note_email_text, :default_estimate_bottom_text, :default_estimate_top_text, :default_estimate_email_subject, :default_estimate_email_text, :default_invoice_bottom_text, :default_invoice_top_text, :default_invoice_email_subject, :default_invoice_email_text, :created_at, :updated_at]
  attr_accessor *ATTRIBUTES

  #
  # Instance methods
  #

  def initialize(attributes)
    self.attributes = attributes
  end

end
