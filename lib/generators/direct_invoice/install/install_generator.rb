class DirectInvoice::InstallGenerator < Rails::Generators::Base

  source_root File.expand_path('../../../../../', __FILE__)

  def copy_initializers
    @source_paths = nil # reset it for the find_in_source_paths method

    DirectInvoice::InstallGenerator.source_root(File.expand_path('../templates', __FILE__))

    template 'direct_invoice.rb', 'config/initializers/direct_invoice.rb'

  end

end
