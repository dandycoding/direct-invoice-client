#Direct Invoice
-------------

##Installation

1.  Add the gem to your Gemfile:

    gem 'direct_invoice'

2. Generate the initializer

    rails g direct_invoice:install
